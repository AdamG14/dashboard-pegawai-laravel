@extends ('master')

<!-- Menampilkan navbar -->
@section ('navbar')
    <!-- Input -->
    <div class="col col-3">
        <a href="pegawai/tambah" class="btn btn-primary">Input Pegawai Baru</a>
    </div> 
    <!-- Search -->
    <div class="col col-6">
        <form class="form-inline" action="/pegawai/search" method="get">
            <input class="form-control mr-sm-0 col col-7" type="search" placeholder="Search" aria-label="Search" name="name">
            <button class="btn btn-outline-success my-0 my-sm-0 col col-3" type="submit">Search</button>
        </form>
    </div>
    <!-- Auth + Home -->
    <div class="col col-3">
        <a href="/logout" class="btn btn-danger" onClick="return confirm('Apakah yakin untuk logout?')">Logout</a>
        <a href="/register" class="btn btn-primary">Register</a>
        <a href="/pegawai" class="btn btn-primary">Home</a>
    </div>
@endsection

<!-- Menampilkan read/content pegawai -->
@section ('content')
    <thead>
        <tr>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Pekerjaan</th>
            <th>OPSI</th>
        </tr>
    </thead>
    <tbody>
        @foreach($pegawai as $p)
        <tr>
            <td>{{ $p->nama }}</td>
            <td>{{ $p->alamat }}</td>
            <td>{{ $p->job->nama }}</td>
            <td>
                <a href="/pegawai/edit/{{ $p->id }}" class="btn btn-warning">Edit</a>
                <a href="/pegawai/delete/{{ $p->id }}" class="btn btn-danger" onClick="return confirm('Apakah yakin untuk hapus pegawai?')">Hapus</a>
            </td>
        </tr>
        @endforeach
    </tbody>
@endsection

<!-- Menampilkan berapa kali halaman telah diakses -->
@section ('logging')
    {{ $pegawai->links() }}
    <p>Halaman ini telah diakses sebanyak {{ $access }}</p>
@endsection

<style>
    .form-control {
        width: 30%;
        display: inline;
    }

    .user-action {
        float: right;
    }
</style>