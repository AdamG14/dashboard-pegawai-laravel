<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <title>Data Pegawai</title>
</head>
<body>
    <div class="container">
        <div class="card mt-5">
            <div class="card-header text-center">
                CRUD Data Pegawai - Training DOT Laravel
                @if (Session::has('alert'))
                <div class="alert alert-success alert-dismissible">
                    <a href="pegawai"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
                    {{Session::get('alert')}}
                </div>
                @endif
                @if (Session::has('alert-delete'))
                <div class="alert alert-danger alert-dismissible">
                    <a href="pegawai"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
                    {{Session::get('alert-delete')}}
                </div>
                @endif
            </div>
            <div class="card-body">
                @yield('login')
                @yield('edit')
                @yield('create')
                @yield('register')

                <div class="row">
                    @yield('navbar')
                </div>

                <table class="table table-bordered table-hover table-striped">
                    @yield('content')
                </table>
                
                @yield('logging')
            </div>
        </div>
    </div>
</body>
</html>