@extends ('master')
@section ('register')
    <a href="/pegawai" class="btn btn-primary">Kembali</a>
    <br/>
    <br/>
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ url('/registerPost') }}" method="post">

        {{ csrf_field() }}

        <div class="form-group">
            <input type="email" name="email" id="email" class="form-control" placeholder="Email...">
        </div>
        <div class="form-group">
            <input type="text" name="username" id="username" class="form-control" placeholder="Username...">
        </div>
        <div class="form-group">
            <input type="password" name="password" id="password" class="form-control" placeholder="Password...">
        </div>
        <div class="form-group">
            <input type="password" name="confirmation" id="password" class="form-control" placeholder="Ketik ulang password...">
        </div>
        <div class="form-group">
            <input type="text" name="name" id="name" class="form-control" placeholder="Name...">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-md btn-primary" onclick="return confirm('Apakah data user sudah benar?')">Submit</button>
        </div>

    </form>
@endsection