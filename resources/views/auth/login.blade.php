@extends ('master')

@section ('login')

@if (\Session::has('alert'))
    <div class="alert alert-danger"><div>{{Session::get('alert-logout')}}</div></div>
@endif

@if (\Session::has('alert-success'))
    <div class="alert alert-success"><div>{{Session::get('alert-success')}}</div></div>
@endif

<form action="{{ url('/loginPost') }}" method="post">

    {{ csrf_field() }}

    <div class="form-group">
        <input type="text" name="username" id="email" class="form-control" placeholder="Username...">
        <div>{{ $errors->first('email') }}</div>
    </div>

    <div class="form-group">
        <input type="password" name="password" id="password" class="form-control" placeholder="Password...">
        <div>{{ $errors->first('password') }}</div>
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-md btn-primary">Login</button>
    </div>

</form>
@endsection