<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\LoginValidation;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function loginView(){
        if(Session::get('login')) {
            return redirect('pegawai');
        } else {
            return view('auth.login');
        }
    }

    public function loginPost(Request $request){
        $username = $request->username;
        $password = $request->password;

        $credentials = User::where('username', $username)->first();
        if($credentials) {
            if(Hash::check($password, $credentials->password)) {
                $token = JWTAuth::fromUser($credentials);
                Session::put('name', $credentials->name);
                Session::put('email', $credentials->email);
                Session::put('username', $credentials->username);
                Session::put('role', $credentials->role);
                Session::put('token', $token);
                Session::put('login', true);
                return redirect('pegawai');
            } else {
                echo("Password Salah");
            }
        } else {
            echo("Tidak terdapat user");
        }
        
        // $credentials = $request->only('email', 'password');
        // if(Auth::attempt($credentials)) {
        //     echo(Auth::user()->username);    
        // }
        // try {
        //     if (! $token = JWTAuth::attempt($credentials)) {

        //         return redirect('login')->with('alert','Password atau Email salah!');

        //     } 
        // } catch (JWTException $e) {

        //     return redirect('login')->with('alert','Gagal membuat token!');
            
        // }
        // return redirect('pegawai');
    }

    public function checkSession(){
        dd(Session::all());
    }

    public function logout(){
        Session::flush();
        return redirect('login')->with('alert-logout','Berhasil Logout!');
    }

    public function register(Request $request){
        return view('auth.register');
    }

    public function registerPost(Request $request){
        // Validasi data yang dimasukkan
        $this->validate($request, [
            'name' => 'required|min:4',
            'username' => 'required|min:4',
            'email' => 'required|min:4|email|unique:users',
            'password' => 'required',
            'confirmation' => 'required|same:password',
        ]);

        // Menyimpan data yang sudah tervalidasi
        $data = new User();
        $data->name = $request->name;
        $data->username = $request->username;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->save();

        Session::flash('alert', 'Sukses tambah user');
        return redirect('pegawai');
    }    
}
