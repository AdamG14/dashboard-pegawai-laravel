<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Pegawai;
use App\Job;

class PegawaiController extends Controller
{

    public function index(Request $request)
    {
        if(!Session::get('login')){
            return redirect('login');
        } else{
            // Mengabil data pegawai serta paginate
            $pegawai = Pegawai::orderBy('id', 'DESC')->paginate(10);
            // Logging
            $total_access = DB::table('access_logs')
            ->where('path', $request->path())
            ->count();

            return view('pegawai.pegawai', ['pegawai' => $pegawai, 'access' => $total_access]);
        }
    }

    public function create()
    {
        $job_id = Job::all();
        return view('pegawai.pegawai_tambah', ['job_id' => $job_id]);
    }

    public function store(Request $request)
    {
        // Validasi data dari form
        $this->validate($request, [
            'nama' => 'required',
            'alamat' => 'required',
            'job' => 'required',
        ]);
        
        // Memasukan data yang sudah tervalidasi
        Pegawai::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'job_id' => $request->job,
        ]);
        
        Session::flash('alert', 'Sukses tambah data');
        return redirect('/pegawai');
    }

    public function edit($id)
    {
        $pegawai = Pegawai::find($id);
        $job_id = Job::all();
        return view('pegawai.pegawai_edit', ['pegawai' => $pegawai, 'job_id' => $job_id]);
    }

    public function update($id, Request $request)
    {
        // Validasi data dari form
        $this->validate($request, [
            'nama' => 'required',
            'alamat' => 'required'
        ]);

        // Update data
        $pegawai = Pegawai::find($id);
        $pegawai->nama = $request->nama;
        $pegawai->alamat = $request->alamat;
        $pegawai->job_id = $request->job;
        $pegawai->save();
        
        Session::flash('alert', 'Sukses edit data');
        return redirect('/pegawai');
    }

    public function delete($id)
    {
        $pegawai = Pegawai::find($id);
        $pegawai->delete();
        
        Session::flash('alert-delete', 'Sukses delete data');
        return redirect('/pegawai');
    }

    public function search(Request $request)
    {
        // Mencari data pegawai berdasarkan nama
        $query = $request->name;
        $pegawai = Pegawai::where('nama', 'LIKE', '%' . $query . '%')
                   ->orderBy('id', 'DESC')
                   ->paginate(10);
        
        // Logging
        $total_access = DB::table('access_logs')
                        ->where('path', $request->path())
                        ->count();

        return view('pegawai.pegawai', ['pegawai' => $pegawai, 'access' => $total_access]);
    }
}
