<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use DateTime;

class AccessLog
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        //Membuat log baru
        DB::table('access_logs')->insert([
            'path' => $request->path(),
            'ip' => $request->getClientIp(),
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);
        return $response;
    }
}
