<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pegawai extends Model
{
    protected $table = "pegawai";   
    protected $fillable = ['nama', 'alamat', 'job_id'];

    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id', 'id');
    }
}
