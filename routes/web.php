<?php

/**
 * Route untuk auth
 */
Route::get('login', 'UserController@loginView')->name('login');
Route::post('loginPost', 'UserController@loginPost');
Route::get('register', 'UserController@register');
Route::post('registerPost', 'UserController@registerPost');
Route::get('logout', 'UserController@logout');
Route::get('checksession', 'UserController@checkSession');

/**
 * Route untuk pegawai
 */
Route::prefix('pegawai')->group(function() {
    Route::middleware('access-log')->group(function() {
        Route::get('', 'PegawaiController@index');
        Route::get('search/', 'PegawaiController@search');
    });

        Route::get('tambah', 'PegawaiController@create');
        Route::get('delete/{id}', 'PegawaiController@delete')->name('delete');    
        Route::get('edit/{id}', 'PegawaiController@edit');
        Route::post('store', 'PegawaiController@store');
        Route::put('update/{id}', 'PegawaiController@update');    
});

// echo route('delete', ['id' => ])